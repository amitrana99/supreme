﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ST.Data.Entities;
using System.Data.Entity.Infrastructure;
using System.Data.Common;
using ST.Data.Mapping;
using System.Data.Entity.Validation;

namespace ST.Data.Context
{
    public class SupremeContext : DbContext, ISupremeContext
    {
        #region Constructor

        public SupremeContext()
            : base("name=ConnectionString")
        {
            //  Db.Connection.Open();
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
        }

        public SupremeContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public SupremeContext(DbConnection existingConnection)
            : base(existingConnection, true)
        {
        }
        #endregion
        #region Table/Views

        public IDbSet<Employee> Employees { get; set; }

        #endregion


            #region Overridable method(s)

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new EmployeeConfiguration());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception.InnerException);
            }
        }

        #endregion


        public Database Db
        {
            get { return this.Database; }
        }

        }
    }


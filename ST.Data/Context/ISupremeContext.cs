﻿using ST.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ST.Data.Context
{
   public interface ISupremeContext : IDisposable
    {
        Database Db { get; }
        IDbSet<Employee> Employees { get; set; }
        int SaveChanges();
    }
}

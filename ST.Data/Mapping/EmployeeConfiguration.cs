﻿using ST.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ST.Data.Mapping
{
   internal class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration(string schema = "Supreme")
        {
            ToTable(schema + ".employee");
            HasKey(x => x.Id);

            Property(x => x.Name).HasColumnName("Namee").IsOptional().HasMaxLength(50);
            Property(x => x.Address).HasColumnName("Address").IsOptional().HasMaxLength(50);
            Property(x => x.DOB).HasColumnName("DOB").IsOptional().HasMaxLength(50);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();
        }
    }
}

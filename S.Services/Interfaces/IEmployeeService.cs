﻿using S.Services.DTOs;
using ST.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Services.Interfaces
{
   public interface IEmployeeService
    {
        EmployeeSaveResponse SaveEmployees(EmployeeSaveRequest request);
        IEnumerable<BaseEmployeeObject> GetEmployeesList();
        BaseResponse DeleteEmployee(int id);
        Employee GetEmployee(int id); 
    }
}

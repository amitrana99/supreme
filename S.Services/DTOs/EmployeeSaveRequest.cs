﻿namespace S.Services.DTOs
{
    public class EmployeeSaveRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }
        public bool IsActive { get; set; }
    }
}

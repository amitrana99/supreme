﻿namespace S.Services.DTOs
{
    public class BaseResponse
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}

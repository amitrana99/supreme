﻿using ST.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Services.DTOs
{
   public class EmployeeSaveResponse : BaseResponse
    {
        public Employee Employee { get; set; } 
    }
}

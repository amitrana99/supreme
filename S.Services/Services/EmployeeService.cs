﻿using S.Services.Interfaces;
using ST.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using S.Services.DTOs;
using ST.Data.Entities;

namespace S.Services.Services
{
   public class EmployeeService:IEmployeeService
    {
        #region Fields

        private readonly ISupremeContext _employeeContext;

        #endregion

        #region Constructor

        public EmployeeService(ISupremeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        #endregion

        //Delete Employee
        public BaseResponse DeleteEmployee(int id)
        {
            var response = new BaseResponse();

            try
            {
                var employee = _employeeContext.Employees.FirstOrDefault(x => x.Id == id);

                if (employee != null)
                {
                    _employeeContext.Employees.Remove(employee);
                    _employeeContext.SaveChanges();
                    response.Success = true;
                    response.Message = "Employee has been successfully deleted.";
                }
                else
                {
                    response.Success = false;
                    response.Message = "An error Occurred : Invalid Parameter.";
                }
            }
            catch (Exception exception)
            {
                response.Success = false;
                response.Message = "An error Occurred :" + exception.Message;
            }
            return response;
        }

        //Get Employee
        public Employee GetEmployee(int id)
        {
            return _employeeContext.Employees.FirstOrDefault(x => x.Id == id);
        }

        //Get EmployeeList
        public IEnumerable<BaseEmployeeObject> GetEmployeesList()
        {
            var tech = _employeeContext.Employees.Select(x => new BaseEmployeeObject
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address,
                DOB = x.DOB,
                IsActive = x.IsActive,
            }).ToList();
            return tech;
        }

        //Save Employee
        public EmployeeSaveResponse SaveEmployees(EmployeeSaveRequest request)
        {
            var response = new EmployeeSaveResponse();

            try
            {
                var employee = _employeeContext.Employees.FirstOrDefault(x => x.Id == request.Id);

                if (employee == null)
                {
                    employee = new Employee          // ADD
                    {
                        Id = request.Id,
                        Name=request.Name,
                        Address=request.Address,
                        DOB=request.DOB,
                        IsActive=request.IsActive
                    };

                    _employeeContext.Employees.Add(employee);
                }
                else    //EDIT
                {
                    employee.Name = request.Name;
                    employee.Address = request.Address;
                    employee.DOB = request.DOB;
                    employee.Address = request.Address;
                    employee.IsActive = request.IsActive;
                }

                _employeeContext.SaveChanges();
                response.Success = true;
                response.Message = "Employee has been successfully saved.";
            }
            catch (Exception exception)
            {
                response.Message = exception.ToString();
            }
            return response;
        }
               
    }
}

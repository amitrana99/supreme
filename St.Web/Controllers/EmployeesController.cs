﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using S.Services.DTOs;
using S.Services.Interfaces;
using St.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace St.Web.Controllers
{
    public class EmployeesController : Controller
    {
        #region Fields

        private readonly IEmployeeService _employeeService;

        #endregion

        #region Constructor

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        #endregion

        #region Methods

        // GET: Employees
        // GET: Admin/Employees
        [Route("~/Employee")]
        public ActionResult Index()
        {
            return View();
        }
        //GET: Employee/Create
        public ActionResult Create()
        {
            var model = new EmployeeModel();
            return View("Create", model);
        }

        //GET: EmployeeList
        [HttpPost]
        [Route("~/Employee/EmployeeList")]
        public ActionResult EmployeeList([DataSourceRequest] DataSourceRequest request)
        {
            var data = GetEmployeeList();
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //EditEmployee
        [HttpPost]
        [Route("~/Employee/EditEmployee/{id}")]
        public JsonResult EditEmployee(int id)
        {
            var response = _employeeService.GetEmployee(id);
            var model = new EmployeeModel
            {
                Id = response.Id,
                Name = response.Name,
                Address = response.Address,
                DOB = response.DOB,
                IsActive = response.IsActive
            };
            return Json(model);
        }

        //DeleteEmployee
        [HttpPost]
        [Route("~/Employee/DeleteEmployee")]
        public ActionResult DeleteEmployee(EmployeeModel model)
        {
            var response = _employeeService.DeleteEmployee(model.Id);
            return Json(new
            {
                success = response.Success,
                message = response.Message
            });
        }

        [HttpPost]
        [Route("~/Employee/SaveEmployee")]
        public ActionResult SaveEmployee(EmployeeModel model)
        {
            var request = Mapper.Map<EmployeeSaveRequest>(model);
            var response = _employeeService.SaveEmployees(request);
            return View("Index");

        }
        #endregion

        #region Private Methods
        private IEnumerable<EmployeeModel> GetEmployeeList()
        {
            var tech = _employeeService.GetEmployeesList();
            var list = tech.Select(x => new EmployeeModel
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address,
                DOB = x.DOB,
                IsActive = x.IsActive
            });
            return list;
        }

        #endregion
    }
}
﻿using Autofac;
using Autofac.Integration.Mvc;
using S.Services.Interfaces;
using S.Services.Services;
using ST.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace St.Web.App_Start
{
    public static class AutofacConfig
    {
        public static void Config()
        {
            // container builder init
            var builder = new ContainerBuilder();

            // Register your web controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            // builder.RegisterWebApiFilterProvider(config);


            //register database context
            builder.RegisterType<SupremeContext>().As<ISupremeContext>();

            //register services
            builder.RegisterType<EmployeeService>().As<IEmployeeService>();


            //builder.RegisterModule(new NLogLoggerAutofacModule());

            //register Web components
            //builder.RegisterType<UserAuthenticator>().As<IUserAuthenticator>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            // Set the dependency resolver for MVC.
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);
        }
    }
}
﻿using System.Web;
using System.Web.Optimization;

namespace St.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // CSS style (kendo/css)
            bundles.Add(new StyleBundle("~/Content/kendo/styles").Include(
                      "~/Content/kendo/kendo.common.min.css",
                      "~/Content/kendo/kendo.metro.min.css"));

            // CSS style (kendo/js)
            bundles.Add(new ScriptBundle("~/bundles/kendo/js").Include(
                      "~/Scripts/kendo/kendo.all.min.js",
                      "~/Scripts/kendo/kendo.aspnetmvc.min.js"));
            // login 
            bundles.Add(new ScriptBundle("~/Scripts/app/login").Include(
                      "~/Scripts/app/login.js"));

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.min.js"));

            // jQueryUI CSS
            bundles.Add(new ScriptBundle("~/Scripts/plugins/jquery-ui/jqueryuiStyles").Include(
                        "~/Scripts/plugins/jquery-ui/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/Assets/source/css").Include(
                      "~/source/css/site.css"));

            bundles.Add(new ScriptBundle("~/Assets/source/js").Include(
                      "~/source/js/site.js"));

            // App scripts
            bundles.Add(new ScriptBundle("~/bundles/apps").Include(
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/custom.js"));

            // validate 
            bundles.Add(new ScriptBundle("~/plugins/validate").Include(
                       "~/Scripts/plugins/validate/jquery.form.js",
                      "~/Scripts/plugins/validate/jquery.validate.min.js",
                      "~/Scripts/plugins/validate/additional-methods.min.js"));

            // CSS style (kendo/css)
            bundles.Add(new StyleBundle("~/Content/kendo/styles").Include(
                      "~/Content/kendo/kendo.common.min.css",
                      "~/Content/kendo/kendo.metro.min.css"));

            // CSS style (kendo/js)
            bundles.Add(new ScriptBundle("~/bundles/kendo/js").Include(
                      "~/Scripts/kendo/kendo.all.min.js",
                      "~/Scripts/kendo/kendo.aspnetmvc.min.js"));

            // toastr notification 
            bundles.Add(new ScriptBundle("~/Scripts/plugins/toastr/js").Include(
                      "~/Scripts/plugins/toastr/toastr.min.js"));

            // toastr notification styles
            bundles.Add(new StyleBundle("~/Content/plugins/toastr/toastrStyles").Include(
                      "~/Content/plugins/toastr/toastr.min.css"));
        }
    }
}

﻿using AutoMapper;
using S.Services.DTOs;
using St.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace St.Web.App_Start
{
    public class AutomapConfig
    {
        public static void Setup()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<EmployeeModel, EmployeeSaveRequest>().ReverseMap();
            });
        }
    }
}
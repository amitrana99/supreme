﻿(function () {

    'use strict';
    var employeePopup = function (id) {
        $("#modal-save-employee-title").html("Add Employee");
        $("#modal-add-employee-form").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });

        return false;
    }
    //delete employee
    $(document).on('click', '.del-Employee', function () {
        debugger;
        var id = $(this).data("id");
        $("#hdnId").val(id);

        $("#modal-del-employee").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });

    //delete single customer
    $("#btnModalSubmit").click(function () {
        var customerId = $("#hdnId").val();
        var buttonText = $("#btnModalSubmit").html();
        $("#btnModalSubmit").attr('disabled', '').html('<i class="fa fa-spinner fa-spin"></i>&nbsp; ' + buttonText);
        debugger;
        $.ajax({
            url: '/Customer/DeleteCustomer',
            type: "POST",
            data: { CustomerId: customerId },
            success: function (result) {
                debugger;
                if (result.success) {
                    Supreme.Util.Notification(true, "Selected employee have been successfully deleted.");
                    $("#modal-del-employee").modal("hide");
                    //refresh grid
                    var grid = $("#EmployeeGrid").data("kendoGrid");
                    $("#txtPassword").show();
                    $("#Password").show();
                    grid.dataSource.read();
                    $("#btnModalSubmit").attr('disabled', null).html('Submit');
                }
                else {
                    Supreme.Util.HandleLogout(result.Message);
                    Supreme.Util.Notification(false, "Some error occured while deleting employee.");
                    $("#btnModalSubmit").attr('disabled', null).html('Submit');
                }
            },
            error: function (xhr, status, error) {
                Supreme.Util.Notification(false, error);
                $("#btnModalSubmit").attr('disabled', null).html('Submit');
            }
        });
    });

    // edit customer
    $(document).on('click', '.edit-Employee', function () {
        debugger;
        var id = $(this).data("id");
        $("#Id").val(id);
        $("#modal-employee").modal("show");
        return false;
    });

    //function to show save employee modal during  edit
    var edit = function (id) {
        $.ajax({
            type: "POST",
            url: "/Employee/EditEmployee/" + id,
            processdata: false,
            async: true,
            dataType: 'json',
            success: function (result) {
                debugger;
                $("#Id").val(result.Id);
                $("#Name").val(result.Name);
                $("#Address").val(result.Address);
                $("#DOB").val(result.DOB);
                if (result.IsActive) {
                    $("#IsActive").prop('checked', true);
                }
                else {
                    $("#IsActive").prop('checked', false);
                }
                $("#modal-employee-form-title").html("Edit Employee")
                $("#modal-add-employee-form").modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                });
            },
            error: function (xhr, status, error) {
                Supreme.Util.Notification(false, "Error");
            }
        });
        return false;
    }

    //edit user
    $(document).on('click', '.edit-Employee', function () {
        debugger;
        var id = $(this).data("id");
        edit(id);
    });

    //view customer's detail
    $(document).on('click', '.view-employee', function () {
        debugger;
        var id = $(this).data("id");
        $("#divEmployeeDetails").html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Please wait..');
        $.ajax({
            url: '/Employee/GetEmployeeDetail/' + id,
            type: 'POST',
            success: function (result) {
                debugger;
                $("#divEmployeeDetails").html(result);
                $('html,body').animate({
                    scrollTop: $("#divEmployeeDetails").offset().top
                }, 'slow');
            },
            error: function (xhr, status, error) {
                debugger;
                $("#divEmployeeDetails").html('No data found.');
            }
        });
        return false;
    });

    //edit customer
    $(document).on('click', '.employees-permission', function () {

        $('#left_All_1').trigger("click");

        var id = $(this).data("id");
        $("#Id").val(id);

        getPermissionDetail(id);
        return false;
    });

    //delete single/multiple user(s) confirm modal
    $("#btnDeleteEmployee").click(function () {
        var checkedCount = $("#EmployeeGrid input:checked").length;
        if (checkedCount > 0) {
            $("#delete-employee-confirm-modal").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });

        } else {
            Supreme.Util.Notification(false, "Please select at least one delete to delete.");
        }
    });

    var saveCustomerForm = $("#saveEmployeeForm").validate({
        rules: {
            Name: {
                required: true
            },
            Address: {
                required: true
            },
            DOB: {
                required: true
            }
        },
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },



        submitHandler: function (form) {
            debugger;
            var buttonText = $("#btnSaveEmployee").html();
            $("#btnSaveEmployee").attr('disabled', '').html('<i class="fa fa-spinner fa-spin"></i>&nbsp; ' + buttonText);

            $(form).ajaxSubmit({
                success: function (data) {

                    debugger;
                    if (data && data.success) {
                        $("#modal-add-employee-form").modal("hide");
                        //refresh grid
                        var grid = $("#EmployeeGrid").data("kendoGrid");
                        grid.dataSource.read();

                        Supreme.Util.Notification(true, "Employee has been successfully saved.");
                    } else {
                        Supreme.Util.HandleLogout(data.message);
                        $('#btnSaveEmployee .alert-danger').removeClass("hide").find(".error-message").html(data.message);
                        Supreme.Util.Notification(false, "Some error occured while saving current employee.");
                    }

                    $("#btnSaveEmployee").attr('disabled', null).html(buttonText);
                    $(form).resetForm();
                    $("#Id").val(0);
                },
                error: function (xhr, status, error) {
                    $("#btnSaveEmployee").attr('disabled', null).html(buttonText);
                }
            });
            return false;
        }
    });



    $("#modal-add-employee-form").on('hidden.bs.modal', function () {
        saveCustomerForm.resetForm();
        $("#Id").val(0);
        $("#divIsActive").addClass("hide");
        $(".has-error").removeClass("has-error");
    });



    $("#btnAddEmployee").click(function () {
        $("#Id").val(0);

        var Id = $("#Id").val();
        saveEmployeeForm.resetForm();
        $.ajax({
            url: '/Employee/AddEmployee/',
            type: 'POST',
            success: function (result) {
                customerPopup(companyId);
                $('#Id').get(0).selectedIndex = 0;
            }
        });
    });

    $("#modal-import-customers").on('hidden.bs.modal', function () {
        importCustomerForm.resetForm();
        $("#messagebox").html('');
        $("#messagebox").removeClass();
        $("#grid").html('');
        $(".has-error").removeClass("has-error");
    });

}).apply(this, [jQuery]);
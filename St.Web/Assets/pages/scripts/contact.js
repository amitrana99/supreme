var Contact = function () {

    return {
        //main function to initiate the module
        init: function () {
            var map;
            $(document).ready(function () {
                map = new GMaps({
                    div: '#gmapbg',
                    lat: 33.900844,
                    lng: -118.272948
                });
                var marker = map.addMarker({
                    lat: 33.900844,
                    lng: -118.272948,
                    title: 'Loop, Inc.',
                    infoWindow: {
                        content: "<b>Sit 'n Sleep, Inc.</b> 14300 S. Main St. <br/> Gardena, CA 90248"
                    }
                });

                marker.infoWindow.open(map, marker);
            });
        }
    };

}();

jQuery(document).ready(function () {
    Contact.init();
});


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace St.Web.Models
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }
        public bool IsActive { get; set; }
    }
}